# go-tenable

This is a simple go module for interacting with tenable.io v1.0 API

# API coverage

The module currently only covers a subset of the platform and vulnerability management APIs.
Other APIs are not currently supported, but MRs are welcome.

## Platform
- [ ] access control
- [x] assets
- [ ] connectors (partially implemented)
- [ ] exports
- [ ] scheduled exports
- [ ] findings
- [ ] groups
- [ ] users

## Vulnerability Management
- [ ] access groups
- [ ] agent config
- [ ] agent exclusions
- [ ] agent groups
- [ ] agents
- [ ] agent bulk operations
- [ ] assets
- [ ] asset attributes
- [ ] audit log
- [ ] credentials
- [ ] editor
- [ ] exclusions
- [ ] exports
- [ ] file
- [ ] filters
- [ ] folders
- [ ] networks
- [ ] permissions
- [ ] plugins
- [ ] policies
- [ ] scanner groups
- [ ] scanners
- [ ] scans
- [ ] server
- [ ] tags
- [ ] target groups
- [ ] vulnerabilities
- [ ] workbenches
