package api

type TenableExportJob struct {
	UUID string `json:"export_uuid"`
}

type TenableExportStatus struct {
	Status    string  `json:"status"`
	Available []int32 `json:"chunks_available"`
}

type TenableExportList struct {
	Exports []TenableExportListItem `json:"exports"`
}

type TenableExportListItem struct {
	UUID           string `json:"uuid"`
	Status         string `json:"status"`
	TotalChunks    int32  `json:"total_chunks"`
	FinishedChunks int32  `json:"finished_chunks"`
	ItemsPerChunk  int32  `json:"num_assets_per_chunk"`
	Created        int32  `json:"created"`
}
