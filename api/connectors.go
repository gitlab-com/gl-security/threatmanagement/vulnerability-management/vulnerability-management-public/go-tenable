package api

import (
	"encoding/json"
	"fmt"
	"html"
)

// Lists connectors
// Query parameters
// limit type:int32
// The number of records to retrieve. If this parameter is omitted, Tenable.io uses the default value of 1000.

// offset type:int32
// The starting record to retrieve. If this parameter is omitted, Tenable.io uses the default value of 0.

// sort type:string
// The field you want to use to sort the results by along with the sort order. The field is specified first, followed by a colon, and the order is specified second (asc or desc). For example, name:desc would sort results by the name field in descending order.
// If you specify multiple fields, the fields must be separated by commas. For example, name:desc,date_created:asc would first sort results by the name field in descending order and then by the date_created field in ascending order.

// responses:
// 200: parsed into connector_types
// 429: Ratelimited

//Example: api.GetConnectorsList(limit,offset,desc)

func (t *TenableAPI) GetConnectorsList(limit int32, offset int32, sort_type string) (*TenableConnectorList, error) {

	var connectors TenableConnectorList
	internal_limit := 1000
	internal_sort_type := ""

	if limit != 1000 {
		internal_limit = int(limit)
	}

	if sort_type != "" {
		internal_sort_type = fmt.Sprintf("&sort=%s", html.EscapeString(sort_type))
	}

	url := fmt.Sprintf("https://cloud.tenable.com/settings/connectors?limit=%d&offset=%d%s", internal_limit, offset, internal_sort_type)

	respbytes, err := t.Get(url)
	if err != nil {
		return &connectors, err
	}
	err = json.Unmarshal(respbytes, &connectors)

	return &connectors, err

}
