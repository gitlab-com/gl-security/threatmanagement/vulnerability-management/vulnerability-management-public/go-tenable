package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"
)

// Export all assets. Monitors export state and returns once all export chunks are ready and parsed.
//
// https://developer.tenable.com/reference/exports-assets-export-status
//
// Note: Per the Tenable API docs: By default, vulnerability exports will only include vulnerabilities found or fixed within the last 30 days if no time-based filters (indexed_at, last_fixed, last_found, or first_found) are submitted with the request.
func (t *TenableAPI) ExportAllVulns() (TenableVulnList, error) {

	vuln_list := TenableVulnsExportTop{}
	var export_job TenableExportJob

	// get export ID
	export_url := "https://cloud.tenable.com/vulns/export"
	payload := strings.NewReader(`{"num_assets":100}`)
	export_post, err := t.Post(export_url, payload)
	if err != nil {
		return nil, err
	}

	// monitor export ID until finished
	err = json.Unmarshal(export_post, &export_job)
	if err != nil {
		return nil, err
	}

	// a small sleep before checking status
	time.Sleep(time.Second)

	// download each chunk
	status_url := fmt.Sprintf("https://cloud.tenable.com/vulns/export/%s/status", export_job.UUID)

	var waiting_for_export = true
	var export_status TenableExportStatus
	retries := 10

	for waiting_for_export {

		if retries == 0 {
			return nil, errors.New("exceeded maximum error retries when checking export status")
		}

		status_get, err := t.Get(status_url)
		if err != nil {
			retries--
			time.Sleep(time.Second)
			continue
		}

		err = json.Unmarshal(status_get, &export_status)
		if err != nil {
			retries--
			time.Sleep(time.Second)
			continue
		}

		if export_status.Status != "PROCESSING" {
			// job finished or errored, move on
			break
		}

		time.Sleep(5 * time.Second)
	}

	// fetch, then parse chunk and append
	for _, chunk := range export_status.Available {

		// fetch chunk
		var chunk_list = make([]TenableVulnExport, 0)
		chunk_url := fmt.Sprintf("https://cloud.tenable.com/vulns/export/%s/chunks/%d", export_job.UUID, chunk)
		chunk_data, err := t.Get(chunk_url)
		if err != nil {
			return nil, err
		}

		// unmarshal
		err = json.Unmarshal(chunk_data, &chunk_list)
		if err != nil {
			return nil, err
		}

		// append vulns
		for _, vuln := range chunk_list {
			vuln_list.AppendVuln(vuln)
			vuln_list.Total++
		}
	}

	// optionally enrich data?
	return &vuln_list, nil
}
